import { ViewVisitorsComponent } from './view-visitors/view-visitors.component';
import { CompanyComponent } from './company.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

  {
    path:'view-visitors',
    component:ViewVisitorsComponent
  },
  {
    path:'',
    component:CompanyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule { }
 