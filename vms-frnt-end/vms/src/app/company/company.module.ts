import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyRoutingModule } from './company-routing.module';
import { CompanyComponent } from './company.component';
import { ViewVisitorsComponent } from './view-visitors/view-visitors.component';
import { CompanyEmployeesComponent } from './company-employees/company-employees.component';
import { InviteComponent } from './invite/invite.component';

@NgModule({
  declarations: [CompanyComponent, ViewVisitorsComponent, CompanyEmployeesComponent, InviteComponent],
  imports: [
    CommonModule,
    CompanyRoutingModule
  ]
})
export class CompanyModule { }
