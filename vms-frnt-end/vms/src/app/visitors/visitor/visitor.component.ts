import { takeWhile } from 'rxjs/operators';
import { VisitorService } from './../../services/visitor.service';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-visitor',
  templateUrl: './visitor.component.html',
  styleUrls: ['./visitor.component.css']
})
export class VisitorComponent implements OnInit {
  addVisitorForm:FormGroup
  private alive:boolean;
  visitor_id_fk;
  constructor(private fb:FormBuilder,private visitorService: VisitorService,private router:Router,private route:ActivatedRoute) { }

  ngOnInit() {

    this.alive=true;
    this.addVisitorForm = this.fb.group ({
      fname:['',[Validators.required,Validators.maxLength(100)]],
      lname :['',[Validators.required,Validators.maxLength(100)]],
      del_id_fk:['',[Validators.required,Validators.maxLength(10)]], 
      mobile:['',Validators.required], 
      email:['',[Validators.required,Validators.maxLength(20)]],
      reason:['',[Validators.required,Validators.maxLength(100)]], 
      });

      this.route.params.subscribe((params)=>{
        this.visitor_id_fk = params.id;
      });
  }

  saveVisitor(){
    this.visitorService.saveVisitor({...this.addVisitorForm.value,visitor_id_fk:this.visitor_id_fk})
    .pipe(takeWhile(()=>this.alive))
    .subscribe((data)=>{
      if(data.status==="SUCCESS"){
          this.router.navigateByUrl("visitor/visiting-perason");
      }
    });
  }

}
;