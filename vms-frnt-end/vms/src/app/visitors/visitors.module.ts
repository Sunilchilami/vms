import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule  } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VisitorsRoutingModule } from './visitors-routing.module';
import { VisitorsComponent } from './visitors.component';
import { DeliveryComponent } from './delivery/delivery.component';
import { VisitorComponent } from './visitor/visitor.component';
import { VisitingPerasonComponent } from './visiting-perason/visiting-perason.component';

@NgModule({
  declarations: [VisitorsComponent, DeliveryComponent, VisitorComponent, VisitingPerasonComponent],
  imports: [
    CommonModule,
    VisitorsRoutingModule,
    FormsModule,
    ReactiveFormsModule ,
  ]
})
export class VisitorsModule { }
