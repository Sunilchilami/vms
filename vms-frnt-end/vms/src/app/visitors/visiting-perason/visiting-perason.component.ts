import { VisitorService } from './../../services/visitor.service';
import { Router } from '@angular/router';
import { EmployeeService } from './../../services/employee.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-visiting-perason',
  templateUrl: './visiting-perason.component.html',
  styleUrls: ['./visiting-perason.component.css']
})
export class VisitingPerasonComponent implements OnInit,OnDestroy {
  private alive:boolean;
  employeeList;
  fname;
  errorMsg;
  alertMsg="";
  constructor(private visitorService:VisitorService,private employeeService:EmployeeService,private router:Router) { }

  ngOnInit() {
    this.alive=true;

     this.employeeService.getEmployees()
     .pipe(takeWhile(()=>this.alive))
     .subscribe((data)=>{
       console.log(data);
       this.employeeList = data.data;
     });
  }

  search(){
    if(this.fname != ""){
      this.employeeList =this.employeeList.filter((res)=>{
        return res.fname.toLocaleLowerCase().match(this.fname.toLocaleLowerCase());
      });
    }else if(this.fname === ""){
      
      this.ngOnInit();
     
    }
    
  }

  sendMailMsg(fname){
    console.log(fname);
    this.visitorService.sendMailMsg({fname})
.pipe(takeWhile(()=>this.alive))
.subscribe((data)=>{
  console.log(data.status);
  console.log(data.message);
  this.errorMsg=data.message;
  setTimeout(()=>{    
    this.errorMsg=false;
}, 7000);
  
   });
  }


  ngOnDestroy(){
    this.alive=false;
  }

}
