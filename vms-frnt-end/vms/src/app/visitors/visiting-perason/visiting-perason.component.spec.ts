import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitingPerasonComponent } from './visiting-perason.component';

describe('VisitingPerasonComponent', () => {
  let component: VisitingPerasonComponent;
  let fixture: ComponentFixture<VisitingPerasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitingPerasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitingPerasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
