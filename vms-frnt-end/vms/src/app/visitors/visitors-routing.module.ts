import { VisitingPerasonComponent } from './visiting-perason/visiting-perason.component';
import { VisitorComponent } from './visitor/visitor.component';
import { DeliveryComponent } from './delivery/delivery.component';
import { VisitorsComponent } from './visitors.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  
  {
    path:'delivery/:id',
    component:DeliveryComponent
  },
  {
    path:'visitor-detail/:id',
    component:VisitorComponent
  },
  {
    path:'visiting-perason',
    component:VisitingPerasonComponent
  } 
  
  
  ,{
    path:'',
    component:VisitorsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisitorsRoutingModule { }
