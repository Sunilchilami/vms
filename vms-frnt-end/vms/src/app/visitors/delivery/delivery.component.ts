import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { takeWhile } from 'rxjs/operators';
import { VisitorService } from './../../services/visitor.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.css']
})
export class DeliveryComponent implements OnInit,OnDestroy {
  deliveryCompanies;
  private alive:boolean;
  visitor_id_fk;
  addDeliveryForm:FormGroup;
  respMsg;
  errMsg;
  constructor(private visitorService: VisitorService,private fb:FormBuilder,private router:Router,private route:ActivatedRoute) { }

  ngOnInit() {
    this.alive=true;
    this.addDeliveryForm = this.fb.group ({
      fname:['',[Validators.required,Validators.maxLength(100)]], 
      del_id_fk:['',[Validators.required,Validators.maxLength(10)]], 
      mobile:['',[Validators.required,Validators.maxLength(10)]], 
      email:['',[Validators.required,Validators.maxLength(20)]], 
      });

    this.visitorService.getDeliveryCompanies()
    .pipe(takeWhile(()=>this.alive))
    .subscribe((data)=>{
      console.log(data)
     this.deliveryCompanies = data.data;
      
    });

    this.route.params.subscribe((params)=>{
      this.visitor_id_fk = params.id;
    });

  }

  saveVisitor(){
    this.visitorService.saveVisitor({...this.addDeliveryForm.value,visitor_id_fk:this.visitor_id_fk})
    .pipe(takeWhile(()=>this.alive))
    .subscribe((data)=>{
       if(data.status==="SUCCESS"){
        this.respMsg=data.message;
          setTimeout(()=>{    
            this.respMsg=false;
        }, 3000);
  
     }
     if(data.status==="Error"){
      this.errMsg=data.message;
        setTimeout(()=>{    
          this.errMsg=false;
      }, 3000);

   }
    });
  }

  ngOnDestroy(){
    this.alive=false;
  }

}
