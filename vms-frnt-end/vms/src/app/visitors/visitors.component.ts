import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VisitorService } from './../services/visitor.service';
import { takeWhile } from 'rxjs/operators';
import { Component, OnInit,OnDestroy} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-visitors',
  templateUrl: './visitors.component.html',
  styleUrls: ['./visitors.component.css']
})
export class VisitorsComponent implements OnInit,OnDestroy {
  visitorReason;
  private alive:boolean;
  visitor_id;
  checkVisitorForm:FormGroup;
  constructor(private visitorService: VisitorService,private fb:FormBuilder,private router:Router) { }

  ngOnInit() {

this.checkVisitorForm = this.fb.group({
  visitor_id:['',[Validators.required]]
});

    this.alive=true;

    this.visitorService.getVisitorReason()
    .pipe(takeWhile(()=>this.alive))
    .subscribe((data)=>{
      console.log(data)
     this.visitorReason = data.data;
      
    });
    
  }


  checkVisitor(){
    // console.log(this.checkVisitorForm.value);
    this.visitor_id=this.checkVisitorForm.value;
    console.log(this.visitor_id.visitor_id);
     if( this.visitor_id.visitor_id==1){
      this.router.navigateByUrl("visitor/visitor-detail/"+this.visitor_id.visitor_id);
     }
    if(this.visitor_id.visitor_id==2){
      this.router.navigateByUrl("visitor/visitor-detail/"+this.visitor_id.visitor_id);
     }
     else if(this.visitor_id.visitor_id==3){
      this.router.navigateByUrl("visitor/delivery/"+this.visitor_id.visitor_id);
     }
  }

  ngOnDestroy(){
    this.alive=false;
  }

}
