import { EmployeeService } from './../../../services/employee.service';
import { CompanyService } from './../../../services/company.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeWhile } from 'rxjs/operators';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit ,OnDestroy{
  addEmployeeForm:FormGroup;
  private alive:boolean;
  companyList;
  constructor(private employeeService:EmployeeService,private fb:FormBuilder,private router:Router) { }

  ngOnInit() {
    this.alive=true;
    this.addEmployeeForm = this.fb.group ({
      c_id_fk:['',Validators.required], 
      fname:['',[Validators.required,Validators.maxLength(100)]], 
      lname:['',[Validators.required,Validators.maxLength(100)]], 
      dob:['',Validators.required], 
      bld_grp:['',[Validators.required,Validators.maxLength(100)]], 
      location:['',[Validators.required,Validators.maxLength(100)]], 
      pincode:['',[Validators.required,Validators.maxLength(10)]], 
      mobile:['',[Validators.required,Validators.maxLength(10)]], 
      email:['',[Validators.required,Validators.maxLength(20)]], 
      gender:['',Validators.required], 
    });

    this.employeeService.getCompanyList()
    .pipe(takeWhile(()=>this.alive))
    .subscribe((data)=>{
     this.companyList = data.data;
      
    });
  }

  saveEmployee(){
    this.employeeService.saveEmployee({...this.addEmployeeForm.value})
    .pipe(takeWhile(()=>this.alive))
    .subscribe((data)=>{
      if(data.status==="SUCCESS"){
          this.router.navigateByUrl("admin/view-employee");
      }
    });
  }

  ngOnDestroy(){
    this.alive=false;
  }
}
