import { EmployeeService } from './../../services/employee.service';
import { Router } from '@angular/router';
import { Component, OnInit ,OnDestroy} from '@angular/core';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-view-employee',
  templateUrl: './view-employee.component.html',
  styleUrls: ['./view-employee.component.css']
})
export class ViewEmployeeComponent implements OnInit ,OnDestroy{
  private alive:boolean;
  employeeList;

  constructor(private employeeService:EmployeeService,private router:Router) { }

  ngOnInit() {
    this.alive=true;

     this.employeeService.getEmployees()
     .pipe(takeWhile(()=>this.alive))
     .subscribe((data)=>{
       console.log(data);
       this.employeeList = data.data;
     });
  
  }
ngOnDestroy(){
  this.alive=false;
}
}
