import { EmployeeService } from './../../services/employee.service';
import { Router } from '@angular/router';
import { Component, OnInit ,OnDestroy} from '@angular/core';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-search-bld-donors',
  templateUrl: './search-bld-donors.component.html',
  styleUrls: ['./search-bld-donors.component.css']
})
export class SearchBldDonorsComponent implements OnInit {
  private alive:boolean;
  employeeList;
  bld_grp;
  errorMsg;
  constructor(private employeeService:EmployeeService,private router:Router) { }

  ngOnInit() {
    this.alive=true;

     this.employeeService.getEmployees()
     .pipe(takeWhile(()=>this.alive))
     .subscribe((data)=>{
       console.log(data);
       this.employeeList = data.data;
     });
  
  }


  search(){
    if(this.bld_grp != ""){
      this.employeeList =this.employeeList.filter((res)=>{
        return res.bld_grp.toLocaleLowerCase().match(this.bld_grp.toLocaleLowerCase());
      });
    }else if(this.bld_grp === ""){
      this.ngOnInit();
    }
    
  }

  sendTextMsg(mobile){
console.log(mobile);
this.employeeService.sendTextMsg({mobile})
.pipe(takeWhile(()=>this.alive))
.subscribe((data)=>{
  console.log(data.status);
});
  }

  sendMailMsg(bld_grp){
    console.log(bld_grp);
    this.employeeService.sendMailMsg({bld_grp})
.pipe(takeWhile(()=>this.alive))
.subscribe((data)=>{
  console.log(data.status);
  console.log(data.message);
  this.errorMsg=data.message;
  setTimeout(()=>{    //<<<---    using ()=> syntax
    this.errorMsg=false;
}, 3000);
  
   });
  }


ngOnDestroy(){
  this.alive=false;
}


}
