import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchBldDonorsComponent } from './search-bld-donors.component';

describe('SearchBldDonorsComponent', () => {
  let component: SearchBldDonorsComponent;
  let fixture: ComponentFixture<SearchBldDonorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchBldDonorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBldDonorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
