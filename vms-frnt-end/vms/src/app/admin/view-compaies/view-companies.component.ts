import { Router } from '@angular/router';
import { CompanyService } from './../../services/company.service';
import { Component, OnInit } from '@angular/core';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-view-companies',
  templateUrl: './view-companies.component.html',
  styleUrls: ['./view-companies.component.css']
})
export class ViewCompaniesComponent implements OnInit {
  private alive:boolean;
  companiesList;
  constructor(private companyService:CompanyService,private router:Router) { }

  ngOnInit() {
    this.alive=true;

     this.companyService.getCompanies()
     .pipe(takeWhile(()=>this.alive))
     .subscribe((data)=>{
       console.log(data);
       this.companiesList = data.data;
     });
  
  }
ngOnDestroy(){
  this.alive=false;
}

}
