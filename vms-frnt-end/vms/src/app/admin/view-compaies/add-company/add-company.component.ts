import { CompanyService } from './../../../services/company.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeWhile } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.css']
})
export class AddCompanyComponent implements OnInit,OnDestroy {
  addCompanyForm:FormGroup;
  private alive:boolean;

  constructor(private companyService:CompanyService,private fb:FormBuilder,private router:Router) { }

  ngOnInit() {
    this.alive=true;
    this.addCompanyForm = this.fb.group ({
     
      cname:['',[Validators.required,Validators.maxLength(100)]], 
      ceo:['',[Validators.required,Validators.maxLength(10)]], 
      phone:['',[Validators.required,Validators.maxLength(10)]], 
      email:['',[Validators.required,Validators.maxLength(20)]], 
      });
  }

  saveCompany(){
    this.companyService.saveCompany({...this.addCompanyForm.value})
    .pipe(takeWhile(()=>this.alive))
    .subscribe((data)=>{
      if(data.status==="SUCCESS"){
          this.router.navigateByUrl("admin/view-companies");
      }
    });
  }

  ngOnDestroy(){
    this.alive=false;
  }
}
