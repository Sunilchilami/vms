import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCompaiesComponent } from './view-compaies.component';

describe('ViewCompaiesComponent', () => {
  let component: ViewCompaiesComponent;
  let fixture: ComponentFixture<ViewCompaiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewCompaiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCompaiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
