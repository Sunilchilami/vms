import { ViewCompaniesComponent } from './view-compaies/view-companies.component';

import { ViewEmployeeComponent } from './view-employee/view-employee.component';

import { ViewVisitorsComponent } from './view-visitors/view-visitors.component';
import { AdminComponent } from './admin.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchBldDonorsComponent } from './search-bld-donors/search-bld-donors.component';
import { AddCompanyComponent } from './view-compaies/add-company/add-company.component';
import { AddEmployeeComponent } from './view-employee/add-employee/add-employee.component';

const routes: Routes = [
  
  {
    path:'view-companies',
    component:ViewCompaniesComponent
  },
  {
    path:'view-companies/add-company',
    component:AddCompanyComponent
  },
  {
    path:'view-companies/update-company/:hcode',
    component:AddCompanyComponent
  }
  ,
  {
    path:'view-employee/add-employee',
    component:AddEmployeeComponent
  },
  {
    path:'view-employee/update-employee/:hcode',
    component:AddEmployeeComponent
  }
  ,
  {
    path:'view-employee',
    component:ViewEmployeeComponent
  }
  ,
  {
    path:'search-bld-grp',
    component:SearchBldDonorsComponent
  }
  ,
  {
    path:'view-visitors',
    component:ViewVisitorsComponent
  }
  
  ,
  {
    path:'',
    component:AdminComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
