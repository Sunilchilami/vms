import { ViewCompaniesComponent } from './view-compaies/view-companies.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';

import { ViewVisitorsComponent } from './view-visitors/view-visitors.component';
import { SearchBldDonorsComponent } from './search-bld-donors/search-bld-donors.component';
import { ViewEmployeeComponent } from './view-employee/view-employee.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AddCompanyComponent } from './view-compaies/add-company/add-company.component';
import { AddEmployeeComponent } from './view-employee/add-employee/add-employee.component';

@NgModule({
  declarations: [AdminComponent, AddCompanyComponent, AddEmployeeComponent, ViewVisitorsComponent, SearchBldDonorsComponent, ViewEmployeeComponent, ViewCompaniesComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class AdminModule { }
