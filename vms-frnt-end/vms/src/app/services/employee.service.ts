import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestAPIService } from './restapi.service';


@Injectable({providedIn: 'root'})
export class EmployeeService {
  constructor(private http: HttpClient,private restApi:RestAPIService) { }

  saveEmployee(values:any){
    console.log(values);
   return this.http.post<{status:string,message:string}>(this.restApi.API_URL+'saveEmployee',values);
  }//saveEmployee

  getCompanyList(){
    return this.http.get<{status:string,message:string,data:any}>(this.restApi.API_URL+'getCompanyList');
  }

  getEmployees(){
    return this.http.get<{status:string,message:string,data:any}>(this.restApi.API_URL+'getEmployees');
  }//getEmployees

  sendTextMsg(mobile){
    console.log(mobile);
   return this.http.post<{status:string,message:string}>(this.restApi.API_URL+'sendTextMsg',mobile);
  }//sendTextMsg

  sendMailMsg(bld_grp){
    console.log(bld_grp);
   return this.http.post<{status:string,message:string}>(this.restApi.API_URL+'sendMailMsg',bld_grp);
  }

}