import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestAPIService } from './restapi.service';


@Injectable({providedIn: 'root'})
export class CompanyService {
  constructor(private http: HttpClient,private restApi:RestAPIService) { }

  saveCompany(values:any){
    console.log(values);
   return this.http.post<{status:string,message:string}>(this.restApi.API_URL+'saveCompany',values);
  }//saveCompany

  getCompanies(){
    return this.http.get<{status:string,message:string,data:any}>(this.restApi.API_URL+'getCompanies');
  }//getCompanies

}