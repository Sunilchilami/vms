import { LogInService } from './../services/login.service';
import { Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { takeWhile } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  logInForm:FormGroup;
  private alive:boolean;

  constructor(private logInService:LogInService,private fb:FormBuilder,private router:Router) { }

  ngOnInit() {
    this.alive=true;
    this.logInForm = this.fb.group ({
     
      cname:['',[Validators.required,Validators.maxLength(100)]], 
      ceo:['',[Validators.required,Validators.maxLength(10)]], 
      phone:['',[Validators.required,Validators.maxLength(10)]], 
      email:['',[Validators.required,Validators.maxLength(20)]], 
      });
  }

  saveCompany(){
    this.logInService.authonticationUser({...this.logInForm.value})
    .pipe(takeWhile(()=>this.alive))
    .subscribe((data)=>{
      if(data.status==="admin"){
          this.router.navigateByUrl("admin");
      }
      else if(data.status==="user"){
        this.router.navigateByUrl("visitor");
    }
    if(data.status==="company"){
      this.router.navigateByUrl("company");
  }
    });
  }

  ngOnDestroy(){
    this.alive=false;
  }
}

