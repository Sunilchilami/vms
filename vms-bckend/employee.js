const mysql = require('mysql2');
const db = require('./db/db');

var connection = db.getDBConnection();
connection.connect();

var saveEmployee=(req,resp)=>{
  //  var connection = db.getDBConnection();
    connection.connect();
  var fname= req.body.fname;
  var lname= req.body.lname;
  var mobile= req.body.mobile;
  var email= req.body.email;
  var c_id_fk= req.body.c_id_fk;
  var dob= req.body.dob;
  var bld_grp= req.body.bld_grp;
  var location= req.body.location;
  var pincode= req.body.pincode;
  var hcode= req.body.hcode;
  var gender= req.body.gender;


var save_qry = `INSERT INTO dat_employee( c_id_fk, fname, lname, dob, bld_grp, location, pincode, mobile, email, hcode, gender ) VALUES (${c_id_fk},'${fname}','${lname}','${dob}','${bld_grp}','${location}','${pincode}','${mobile}','${email}', '${hcode}','${gender}' )`;
    console.log(save_qry);
    connection.query(save_qry ,  function(err, results) {

        if (err) {
            resp.json({
                status: "Error",
                message:"Not Saved"
            });

        } else {
            resp.status(200).json({
                status: "SUCCESS",
                message:"saved successfully",
                data:results
            });

        }

    });
} //saveVisitor

getEmployees = (req,resp) => {
    connection.connect();
    var getQry = `SELECT t1.id,t1.fname,t1.lname,t1.dob,t1.bld_grp,t1.location,t1.pincode,t1.mobile,t1.email,t1.gender,t2.cname FROM dat_employee t1 inner join dat_company t2 on t1.c_id_fk=t2.id;
`;
    connection.query(getQry, function(err,result){
      if(err){
        console.log("err");
      }else{
        resp.status(200).json({
          status: "SUCCESS",
          data:result
        });
      }
    });
}

getCompanyList = (req,resp) => {
    connection.connect();
    var getQry = `SELECT id,cname FROM dat_company`;
    connection.query(getQry, function(err,result){
      if(err){
        console.log("err");
      }else{
        resp.status(200).json({
          status: "SUCCESS",
          data:result
        });
      }
    });
}

module.exports={getEmployees, saveEmployee,getCompanyList};
