const http=require('http');
const express=require('express');
const parser=require('body-parser');
const status = require('./db/db');
const signup = require('./signup');
const visitor = require('./visitor');
const employee = require('./employee');
const company = require('./company');

var app=express();
var port=9999;


app.use((req,resp,next)=>{
    resp.setHeader('Access-Control-Allow-Origin',"*");
    resp.setHeader('Access-Control-Allow-Headers',"Origin,X-Requested-With, Content-Type,Accept");
    resp.setHeader('Access-Control-Allow-Methods',"GET,POST,PATCH, DELETE, OPTIONS");
    next();
});

app.use(parser.json());

app.set('port',port);
const server=http.createServer(app);

server.listen(port,()=>{
    console.log('chaalo ayti nodrii '+ port);
});

app.post('/api/createUser',(req,resp,next)=>{

    signup.createUser(req,resp);
});

app.post('/api/userAuth',(req,resp,next)=>{
     signup.userAuth(req,resp);
});

app.post('/api/saveVisitor',(req,resp,next)=>{
  visitor.saveVisitor(req,resp);
});

app.get('/api/getVisitors',(req,resp,next)=>{
  visitor.getVisitors(req,resp);
});

app.get('/api/getVisitorReason',(req,resp)=>{
  visitor.getVisitorReason(req,resp);
});

app.post('/api/sendMailMsgToContactee',(req,resp,next)=>{
  visitor.sendMailMsgToContactee(req,resp);
});


app.get('/api/getDeliveryCompanies',(req,resp)=>{
  visitor.getDeliveryCompanies(req,resp);
});

app.post('/api/saveEmployee',(req,resp,next)=>{
  employee.saveEmployee(req,resp);
});

app.get('/api/getEmployees',(req,resp)=>{
  employee.getEmployees(req,resp);
});

app.get('/api/getCompanyList',(req,resp)=>{
  employee.getCompanyList(req,resp);
});



app.post('/api/saveCompany',(req,resp,next)=>{
  company.saveCompany(req,resp);
});

app.get('/api/getCompanies',(req,resp)=>{
  company.getCompanies(req,resp);
});

app.post('/api/sendTextMsg',(req,resp,next)=>{
  company.sendTextMsg(req,resp);
});

app.post('/api/sendMailMsg',(req,resp,next)=>{
  company.sendMailMsg(req,resp);
});
